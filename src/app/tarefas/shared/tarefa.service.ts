/**
 * Serviço responsável por executar as operações de CRUD.
 *
 * @author Márcio O. Corbolan<marciocorbolan@hotmail.com>
 * @since 1.0.0
 */

import { Injectable } from '@angular/core';

import { Tarefa } from './tarefa.model';

@Injectable()
export class TarefaService {

  constructor() { }

  /**
   * Método responsável por listar todos registros.
   *
   * @return Tarefa[] Lista com todas os registros.
   */
  listarTodos(): Tarefa[] {
    const tarefas = localStorage['tarefas'];

    return tarefas ? JSON.parse(tarefas) : [];
  }

  /**
   * Método responsável por cadastrar um registro.
   *
   * @param tarefa Tarefa
   * @return Tarefa Registro cadastrado
   */
  cadastrar(tarefa: Tarefa): Tarefa {
    const tarefas = this.listarTodos();
    tarefa.id = new Date().getTime();
    tarefas.push(tarefa);
    localStorage['tarefas'] = JSON.stringify(tarefas);

    return tarefa;
  }

  /**
   * Método responsável por exibir um registro.
   *
   * @param id number
   * @return Tarefa Registro cadastrado
   */
  buscarPorId(id: number): Tarefa {
    const tarefas: Tarefa[] = this.listarTodos();

    return tarefas.find(tarefa => tarefa.id === id);
  }

  /**
   * Método responsável por atualizar um registro.
   *
   * @param tarefa Tarefa
   * @return Tarefa Registro atualizado
   */
  atualizar(tarefa: Tarefa): Tarefa {
    const tarefas: Tarefa[] = this.listarTodos();
    tarefas.forEach((obj, index, objs) => {
      if (tarefa.id === obj.id) {
        objs[index] = tarefa;
      }
    });
    localStorage['tarefas'] = JSON.stringify(tarefas);

    return tarefa;
  }

  /**
   * Método responsável por remover um registro.
   *
   * @param id number
   * @return void
   */
  remover(id: number): void {
    let tarefas: Tarefa[] = this.listarTodos();
    tarefas = tarefas.filter(tarefa => tarefa.id !== id);
    localStorage['tarefas'] = JSON.stringify(tarefas);
  }

  /**
   * Método responsável por alterar o status de um registro.
   *
   * @param id number
   * @return Tarefa Registro atualizado
   */
  alterarStatus(id: number): Tarefa {
    const tarefas: Tarefa[] = this.listarTodos();
    let tarefa = new Tarefa();
    tarefas.forEach((obj, index, objs) => {
      if (id === obj.id) {
        objs[index].concluida = !obj.concluida;
        tarefa = objs[index];
      }
    }, tarefa);
    localStorage['tarefas'] = JSON.stringify(tarefas);

    return tarefa;
  }
}
